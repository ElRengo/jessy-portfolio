export default (ms: number) => {
    return `${ms / 1000}s`
}
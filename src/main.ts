import { createApp } from "vue";
import { createPinia } from "pinia";
import FontAwesomeIcon from './plugins/fontawesome'

import "./assets/index.css";

import App from "./App.vue";
import router from "./router";
import { setupI18n } from "./locales/setupI18n";

const app = createApp(App).component('font-awesome-icon', FontAwesomeIcon);

setupI18n(app);
app.use(createPinia());
app.use(router);

app.mount("#app");

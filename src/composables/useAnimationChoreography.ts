import type { InjectionKey, Ref } from "vue";
import { clamp, isNumber } from "@vueuse/core";

export const headingsAnimationDuration = 1600;
export const shiftUpAnimationDuration = 1000;

const currentIndexContextKey: InjectionKey<number> = Symbol();

export const createIndexContext = (index: number) => provide(currentIndexContextKey, index);

export const useCurrentIndexContext = () => inject(currentIndexContextKey);

export interface ElementAnimationContextProps {
    canApplyAnimation: Ref<boolean>;
}

const currentElementAnimationContextKey: InjectionKey<ElementAnimationContextProps> = Symbol();

export const createElementAnimationContext = (props: ElementAnimationContextProps) => {
    provide(currentElementAnimationContextKey, props)
};

export const useElementAnimationContext = () => inject(currentElementAnimationContextKey);

export const useAnimationProgression = (duration: number) => () => {
    const isStarted = ref(true);
    const isFinished = ref(false);
    const progress = ref(0);
    const leftDuration = computed(() => (1 - progress.value) * duration);
    let currentDuration: number = duration;
    let endAt: number;
    let startAt = Date.now();

    endAt = startAt + currentDuration;
    if (endAt === startAt) {
        isStarted.value = false;
        isFinished.value = true;
        progress.value = 1;
    } else {
        useRafFn(() => {
            const now = Date.now();
            progress.value = clamp(1 - (endAt - now) / currentDuration, 0, 1);
            if (progress.value >= 1 && !isFinished.value) {
                isStarted.value = false;
                isFinished.value = true;
            }
        });
    }
    return {
        duration,
        isFinished,
        isStarted,
        progress,
        leftDuration,
    }
}

const useHeadingsAnimationProgression = createSharedComposable(useAnimationProgression(headingsAnimationDuration));

export function useAnimationChoreography(canApplyAnimation?: Ref<boolean>) {
    const shiftUpAnimationDelay = ref(0);
    const textRevealAnimationDelay = ref(0);
    const borderRevealAnimationDelay = ref(0);

    const index = useCurrentIndexContext();
    const { leftDuration: headingsAnimationLeftDuration } = useHeadingsAnimationProgression();
    let shiftUpAnimationDelayProgression = useAnimationProgression(shiftUpAnimationDelay.value)();
    const updateCSSChoreographyVariables = () => {
        shiftUpAnimationDelay.value = headingsAnimationLeftDuration.value + (index ?? 0 * 0.2);
        shiftUpAnimationDelayProgression = useAnimationProgression(shiftUpAnimationDelay.value)();
        const shiftUpAnimationDelayLeftDuration = shiftUpAnimationDelayProgression.leftDuration.value;
        textRevealAnimationDelay.value = shiftUpAnimationDelayLeftDuration;
        if (isNumber(index)) {
            borderRevealAnimationDelay.value = shiftUpAnimationDelayLeftDuration + shiftUpAnimationDuration * 0.5;
        } else {
            borderRevealAnimationDelay.value = shiftUpAnimationDelayLeftDuration;
        }
    }
    updateCSSChoreographyVariables();

    if (canApplyAnimation?.value === false) {
        watch(canApplyAnimation!, (canApplyAnimation) => {
            if (canApplyAnimation) {
                updateCSSChoreographyVariables();
            }
        });
    }

    return {
        headingsAnimationDuration: convertNumberAsSecondsString(headingsAnimationDuration),
        shiftUpAnimationDelay: computed(() => convertNumberAsSecondsString(shiftUpAnimationDelay.value)),
        textRevealAnimationDelay: computed(() => convertNumberAsSecondsString(textRevealAnimationDelay.value)),
        borderRevealAnimationDelay: computed(() => convertNumberAsSecondsString(borderRevealAnimationDelay.value)),
    };
}

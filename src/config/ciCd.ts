export default {
  headings: ["ciCDStrategy"],
  children: [
    {
      grid: [
        {
          title: "reducingDevelopment",
          desc: {
            text: "iHelpYouToAutomate",
          },
          list: [
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/.gitlab-ci.yml",
              linkText: "buildingAContinuousDeployment",
            },
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/.gitlab-ci.yml#L61",
              linkText: "thatBuildsADockerImage",
            },
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/.gitlab-ci.yml#L62",
              linkText: "pushesItToTheGitLabContainerRegistry",
            },
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/.gitlab-ci.yml#L68",
              linkText: "andDeploysItToYouServer",
            },
          ],
        },
        {
          title: "makingAppAccessible",
          desc: {
            text: "iCanHelpExposing",
            children: [
              {
                link: "https://www.docker.com/",
                linkText: "docker",
                style: "font-bold",
              },
              { text: "and" },
              {
                link: "https://doc.traefik.io/traefik/",
                linkText: "traefik",
                style: "font-bold",
              },
              { text: "or" },
              {
                link: "https://firebase.google.com/",
                linkText: "firebase",
                style: "font-bold",
              },
            ],
          },
          list: [
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/docker-compose.yml#L7",
              linkText: "configuringAndRunningTraefik",
            },
            {
              link: "https://gitlab.com/ElRengo/jessy-portfolio/-/blob/main/docker-compose.prod.yml#L16",
              linkText: "registeringContainersWithTraefik",
            },
            {
              text: "confifuringFirebase",
            },
          ],
        },
      ],
    },
  ],
};

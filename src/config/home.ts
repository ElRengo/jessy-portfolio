export default {
  headings: ["achieveYourBusinessGoals", "throughBuildingGreatApps"],
  caption: "iHelpYou",
  children: [
    {
      title: "whatBenefitIBring",
      section: true,
      grid: [
        {
          richTexts: [
            {
              style: "font-bold",
              linkText: "iCanHelpYouFormYourAndroidAndIOSMobileApps",
              link: "mobile",
            },
            {
              text: "toMoveFromNativeToCrossPlatform",
            },
          ],
        },
        {
          richTexts: [
            {
              style: "font-bold",
              linkText: "iCanHelpYouFormYourWebsite",
              link: "website",
            },
            {
              text: "webSiteTechnologies",
            },
          ],
        },
        {
          richTexts: [
            {
              style: "font-bold",
              linkText: "iCanHelpYouFormYourApp",
              link: "ciCd",
            },
            {
              text: "ciCDCreationTakesTime",
            },
          ],
        },
      ],
    },
    {
      title: "serviceIProvide",
      section: true,
      desc: {
        text: "iProvideTheAboveBenefits",
      },
      list: [
        {
          linkText: "mobileAppDevelopment",
          link: "mobile",
        },
        {
          linkText: "frontEndDevelopment",
          link: "website",
        },
        {
          linkText: "backEndDevelopment",
          link: "website",
        },
        {
          linkText: "ciCDEnvironment",
          link: "ciCd",
        },
      ],
    },
    {
      title: "aPassionateEngineer",
      section: true,
      desc: {
        text: "iAmPassionateAboutUserExperienceConception",
      },
    },
    {
      title: "whatMakesMeQualified",
      section: true,
      desc: {
        children: [{ text: "goodQuestion" }, { text: "someOfThe" }],
      },
      list: [
        { text: "iHaveBeenDoing" },
        {
          text: "iGrowUpAsASoftwareEngineer",
          children: [
            {
              style: "font-bold",
              linkText: "dassaultSystemes",
              link: "https://www.3ds.com/",
            },
          ],
        },
        { text: "iLoveSpeakingAboutBest" },
        { linkText: "iHaveLaunchedMyOwnStartup", link: "https://werin.app/" },
      ],
    },
  ],
};

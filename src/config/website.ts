export default {
  headings: ["developingWebsite", "andFaithfulRendering"],
  children: [
    {
      grid: [
        {
          title: "followingWebStandars",
          desc: {
            text: "iDevelopByFollowing",
          },
        },
        {
          title: "buildingDesignSystem",
          desc: {
            text: "toHaveARendering",
          },
        },
        {
          title: "testingSolutions",
          desc: {
            text: "doesYourOrganization",
          },
          list: [
            {
              text: "prototyping",
            },
            {
              text: "e2eTesting",
            },
            {
              text: "crossBrowserTesting",
              children: [
                {
                  style: "font-bold",
                  linkText: "lookoutFor",
                  link: "https://caniuse.com/",
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};

export default {
  headings: ["optimizingDeliveryOfYourMobileAppToMarket", "withHighProductivityAndCostEfficiency"],
  desc: {
    text: "ifYouAreLooking",
    children: [
      {
        style: "font-bold",
        linkText: "getInTouch",
        link: "mailto:gsiwords@gmail.com",
      },
    ],
  },
  caption: "iAchieveTheseAimsIn_4Ways",
  figure: {
    title: "youCanTryMyMobileApp",
    desc: "availableDownloadOnAppStoreGooglePlay",
    // src: "../assets/download.png",
    alt: "Google play button",
  },
  children: [
    {
      grid: [
        {
          title: "buildingNativeIos",
          desc: {
            text: "iCanBuildNativeIOS",
            children: [
              {
                style: "font-bold",
                linkText: "flutterFrameworks",
                link: "https://flutter.dev/",
              },
              {
                text: "withBuiltInOptimizedPerformances",
              },
            ],
          },
          list: [
            {
              text: "reduceTimeToMarket",
            },
            {
              text: "suitableForAnyTargetPlatformLike",
            },
            {
              text: "perfectForAMinimumViableProductMvp",
            },
            {
              text: "highlyExpressiveUi",
            },
            {
              text: "oneSingleCodeForFrontEndAndBackEnd",
            },
            {
              text: "nativePerformance",
            },
            {
              text: "fasterCodeDevelopment",
            },
          ],
        },
        {
          title: "developingKeyFeatures",
          desc: {
            text: "iCanProvideManyPowerfulFeatures",
          },
          list: [
            ...[
              [
                "socialLogin",
                "https://firebase.google.com/docs/auth",
                "registerOrSignUp",
              ],
              [
                "smsLogin",
                "https://firebase.google.com/docs/auth",
                "smoothLogin",
              ],
              [
                "pushNotification",
                "https://firebase.google.com/docs/cloud-messaging",
                "firebaseCloudMessaging",
              ],
            ].map(([linkText, link, text]) => ({
              children: [
                {
                  style: "font-bold",
                  linkText,
                  link,
                },
                {
                  text,
                },
              ],
            })),
            {
              text: "multiPayments",
            },
            {
              text: "darkLightTheme",
            },
            ...[
              ["advertisement", "https://flutter.dev/monetization", "earnMode"],
              [
                "dynamiqueLink",
                "https://firebase.google.com/docs/dynamic-links",
                "deepLink",
              ],
            ].map(([linkText, link, text]) => ({
              children: [
                {
                  style: "font-bold",
                  linkText,
                  link,
                },
                {
                  text,
                },
              ],
            })),
            {
              text: "performanceCaching",
            },
          ],
        },
        {
          title: "publishingMobileApp",
          desc: {
            text: "publishingMobileAppToGooglePlayAndAppleStore",
          },
        },
        {
          title: "buildingRealtimeAuthenticated",
          desc: {
            text: "iCanDevelopWebAndMobile",
          },
        },
      ],
    },
  ],
};

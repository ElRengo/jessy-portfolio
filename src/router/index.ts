import { createRouter, createWebHistory, type RouteLocationNormalized, type Router } from "vue-router";
import { setupLayouts } from "virtual:generated-layouts";
import generatedRoutes from "virtual:generated-pages";

const routes = setupLayouts(generatedRoutes);
const scrollBackToTop = (router: Router) => {
  router.afterEach(async (to) => {
    window.scrollTo(0, 0);
    return true;
  });
}
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior() {
    return { top: 0 }
  },
});
scrollBackToTop(router);

export default router;

export type RichTextSpan = {
  text?: string;
  link?: string;
  linkText?: string;
  style?: string;
  children?: RichTextSpan[];
};

import type { RichTextSpan } from "./BaseRichText";

export type Article = {
  title?: string;
  desc?: RichTextSpan;
  section?: boolean;
  list?: RichTextSpan[];
  grid?: Article[];
  richTexts?: RichTextSpan[];
};

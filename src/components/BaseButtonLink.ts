
export const buttonProps = {
    textColor: { type: String },
    color: { type: String },
    hoverColor: { type: String },
    outlined: { type: Boolean },
    href: { type: String },
    text: { type: String },
};
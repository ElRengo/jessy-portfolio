import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faB } from '@fortawesome/free-solid-svg-icons'
import { faGithub, faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons'

library.add(faB, faLinkedin, faGithub, faGitlab)

export default FontAwesomeIcon
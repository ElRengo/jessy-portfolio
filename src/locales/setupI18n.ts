import type { App } from "vue";
import type { I18n, I18nOptions } from "vue-i18n";
import { genMessage } from "./helper";
import { createI18n } from "vue-i18n";

const { fallback, availableLocales } = {
  fallback: "en",
  availableLocales: ["en", "fr"],
};

export let i18n: ReturnType<typeof createI18n>;

async function createI18nOptions(): Promise<I18nOptions> {
  const locale = "en";
  let modules = {};
  if (locale == "en") {
    modules = import.meta.globEager(`./en/**/*.ts`);
  }
  const defaultLocal = {
    message: {
      ...genMessage(modules, locale),
    },
    dateLocale: null,
    dateLocaleName: locale,
  };
  const message = defaultLocal.message ?? {};

  return {
    legacy: false,
    locale,
    fallbackLocale: fallback,
    messages: {
      [locale]: message,
    },
    availableLocales: availableLocales,
    sync: true,
    silentTranslationWarn: true,
    missingWarn: false,
    silentFallbackWarn: true,
  };
}

export async function setupI18n(app: App) {
  const options = await createI18nOptions();
  i18n = createI18n(options) as I18n;
  app.use(i18n);
}

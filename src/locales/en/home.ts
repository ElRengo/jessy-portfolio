export default {
  achieveYourBusinessGoals:
    "Achieve Your Business Goals",
  throughBuildingGreatApps: "Through Building Great Apps",
  iHelpYou:
    "I help you show that an accessible, useable and user-first experience is the best way to business success. By covering topics such as good code architecture, design and even ci-cd.",
  whatBenefitIBring: "What Benefit I Bring",
  iCanHelpYouFormYourAndroidAndIOSMobileApps:
    "I can help you form your Android and IOS mobile apps.",
  toMoveFromNativeToCrossPlatform:
    "To move from native to cross-platform development mobile app. To reduce your time-to-market, development time and maintainability costs. That's where I can help.",
  iCanHelpYouFormYourWebsite: "I can help you form your website.",
  webSiteTechnologies:
    "Website technologies are changing very quickly and organizations need to adapt. But that is not always easy to do. I can build robust website with good design system, performance, framework, and so on.",
  iCanHelpYouFormYourApp: "I can help you form your app ci-cd strategy.",
  ciCDCreationTakesTime:
    "CI/CD creation takes time and a lot of effort. It involves hundreds of decisions, discussions, and creation time. I can setup a CI/CD environment that will help to guarantee code quality, fix bugs quickly, and ensure user-centric feature delivery.",
  serviceIProvide: "Service I provide",
  iProvideTheAboveBenefits:
    "I provide the above benefits through a broad range of services.",
  mobileAppDevelopment: "Mobile app development",
  frontEndDevelopment: "Front end development",
  backEndDevelopment: "Back end development",
  ciCDEnvironment: "CI/CD environment",
  whatMakesMeQualified: "What makes me qualified?",
  goodQuestion: "Good question!",
  someOfThe: "Some of the reasons why people choose to work with me:",
  iHaveBeenDoing: "I have been doing this for a long time. Over 6 years now!",
  iGrowUpAsASoftwareEngineer: "I grew up as a Software Engineer at ",
  dassaultSystemes: "Dassault Systèmes",
  whereIhaveSuccessfully: " where I have successfully completed many project",
  iLoveSpeakingAboutBest:
    "I love speaking about best practices in either development, design, or user experience.",
  iHaveLaunchedMyOwnStartup: "I have launched my own startup",
  aPassionateEngineer: "a passionate engineer",
  iAmPassionateAboutUserExperienceConception:
    'I am passionate about user experience conception. I like helping people carry out their project successfully. A thinker and solver, I like solving complex problems with simple solutions. I like designing applications that are robust, secure, have a nice "look and feel" and are easy to use.',
};

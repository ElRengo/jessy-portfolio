export default {
  youCanTryMyMobileApp: "You can try my mobile app",
  availableDownloadOnAppStoreGooglePlay: "Available download on App Store & Google Play",
  optimizingDeliveryOfYourMobileAppToMarket:
    "Optimizing Delivery Of Your Mobile App to Market",
  withHighProductivityAndCostEfficiency: "with High Productivity and Cost-Efficiency",
  ifYouAreLooking:
    "If you are looking for a mobile app with good design, native performance and super-fast loading.",
  getInTouch: "Get in touch",
  iAchieveTheseAimsIn_4Ways: "I achieve these goals in 4 ways:",
  buildingNativeIos: "Building Native IOS & Android Mobile App",
  iCanBuildNativeIOS:
    "I can build native iOS, Android mobile app, Web and Desktop (Mac OS, Windows) in a single codebase with",
  flutterFrameworks: "Flutter Frameworks",
  withBuiltInOptimizedPerformances:
    "with built-in optimized performances:",
  nativePerformance: "Native performance",
  highlyExpressiveUi: "Highly expressive UI",
  oneSingleCodeForFrontEndAndBackEnd:
    "One single code for front end and back end",
  perfectForAMinimumViableProductMvp:
    "Perfect for a minimum viable product (MVP)",
  fasterCodeDevelopment: "Faster code development",
  reduceTimeToMarket: "Reduce time-to-market",
  suitableForAnyTargetPlatformLike:
    "Suitable for any target platform like native iOS/Android mobile app, Web and Desktop (Mac OS, Windows)",
  developingKeyFeatures: "Developing Key Features",
  iCanProvideManyPowerfulFeatures:
    "I can provide many powerful features that will help you deliver your app faster:",
  socialLogin: "Social Login:",
  registerOrSignUp:
    "Register or sign up with many providers (apple, Facebook, google, email)",
  smsLogin: "SMS Login:",
  smoothLogin: "Smooth login via SMS just by using the user's phone number",
  pushNotification: "Push Notification:",
  firebaseCloudMessaging:
    "Firebase cloud messaging (FCM) for sending notification on user phone",
  multiPayments:
    "Multi Payments: Ability to pay with Paypal, Stripe, Razorpay…",
  darkLightTheme:
    "Dark & Light Theme: Ability to change the default screen to Dark Theme.",
  advertisement: "Advertisement:",
  earnMode: "Earn mode revenue with your apps by using Admob, Facebook Ads",
  dynamiqueLink: "Dynamic Link:",
  deepLink: "Deep link potential users to the right place inside your app",
  performanceCaching:
    "Performance Caching: Support many types of Caching methods to speed up your app",
  publishingMobileApp: "Publishing Mobile App",
  publishingMobileAppToGooglePlayAndAppleStore:
    "Publishing mobile app to Google Play and Apple Store takes time and a lot of effort. I can help you to deliver your app to both Google Play and Apple Store (iOS).",
  buildingRealtimeAuthenticated:
    "Building Realtime, Authenticated, Scalable and Secure App",
  iCanDevelopWebAndMobile:
    "I can develop web and mobile applications without having to build your own server-side. I will provide a Firebase setup support to grab critically important features like authentication, file storage, cloud functions, and real-time database. Thus you can move quickly to a world-class cloud infrastructure.",
};

export default {
  developingWebsite:
    "Developing website with attention to detail",
  andFaithfulRendering: "and faithful rendering of the the design",
  followingWebStandars: "Following Web Standards",
  iDevelopByFollowing:
    "I develop by following web standards, usage, and UI testing on browsers. I develop with Vue.js or React.js and in-house frameworks in close collaboration with developers.",
  buildingDesignSystem: "Building Design System",
  toHaveARendering:
    "To have a rendering as close as possible to the model of the web designer, I use Avocode. I can import Photoshop, Adobe XD and Sketch models to have dimensions and information in many languages (sass, less, react…). I'm also familiar with Figma.",
  testingSolutions: "Testing Solutions",
  doesYourOrganization:
    "Does your organization need somebody with automated test expertise? Somebody who understands how tests can help improve your profits? Tests show that bugs are no longer present (not that there are no bugs). But, you get value back, indeed the more often a test runs, the more time it has saved you. Thus, the longer the life cycle of a project, the more critical tests become. Many businesses don’t care about your tests they care about making profits. Tests drive up profits by helping developers deliver higher-quality software faster. I can help you write your automated tests by:",
  prototyping: "Prototyping",
  e2eTesting: "E2E Testing",
  crossBrowserTesting:
    "Cross-browser testing process is iterative. So that I am constantly on the ",
  lookoutFor:
    "lookout for the behavior of the site on the most used browsers on the market.",
};

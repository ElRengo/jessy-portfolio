export default {
  ciCDStrategy: "CI/CD Strategy",
  reducingDevelopment: "Reducing Development Cycles with GitLab CI/CD",
  iHelpYouToAutomate:
    "I help you automate your app whole deployment chain. To gather feedback and publish code changes faster. It will make your development cycles shorter. I do this by:",
  buildingAContinuousDeployment:
    "Building a continuous deployment pipeline with GitLab,",
  thatBuildsADockerImage: "That builds a Docker image,",
  pushesItToTheGitLabContainerRegistry:
    "Pushes it to the GitLab container registry, ",
  andDeploysItToYouServer: "And deploys it to your server using SSH.",
  makingAppAccessible:
    "Making App Accessible with Domain Name and Secure Communication",
  iCanHelpExposing:
    "I can help exposing your application to the rest of the world by using",
  or: " or ",
  and: " and ",
  docker: "Docker",
  traefik: "Traefik",
  firebase: "Firebase",
  iDoThisBy: "I do this by:",
  configuringAndRunningTraefik: "Configuring and Running Traefik",
  registeringContainersWithTraefik: "Registering Containers with Traefik",
  confifuringFirebase: "Configuring Firebase",
};

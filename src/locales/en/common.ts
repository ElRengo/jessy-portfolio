export default {
  getInTouch: "Get in touch",
  curriculumVitae: "Curriculum Vitae (PDF)",
  seeThisWebsiteCode: "See this website code"
};
